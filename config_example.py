
"""
Copy this file to config.py

Edit the file to contain your WIKI credientials and your PG login info.

Run the schemaToRedmine.py (or whatever your wiki is)

./schemaToWiki.py -f config

If the configuration is correct your wiki page should contain the Markdown for
  your schema.
"""

showqueries=False
#connstr='postgresql+psycopg2://dbuser:dbpass@127.0.0.1:5432/dbname'
#connstr='postgresql+psycopg2://yourpguser:yourpgpass@127.0.0.1:5432/yourpgdb'
connstr="mssql+pymssql://username:yourmspass@sqlserverhost/yourmsdb"

wikiBaseUrl="https://myredminewiki.foo.com/"
wikiPostUrl="https://myredminewiki.foo.com/projects/myredmine/wiki/Actual_Schema/edit" 
wikiUsername="yourredmineuser"
wikiPassword="yourredminepass"
