
from schemaToWiki import WikiFormatter, WikiPoster

"""trac wiki support"""

class TracFormatter(WikiFormatter):
    """Format the wiki entry Trac style."""

    def format(self, tabinfo):
        """tabinfo is the data model of the meta data for the tables"""

        schemaLines=[]

        schemaLines.append("= Actual Schema as of %s =" % datetime.now())
        schemaLines.append(" ")

        for tinfo in tabinfo:
            t = tinfo.get('name',"")
            fks = {}

            schemaLines.append( "== %s ==" % t.upper() )
            schemaLines.append( " " )
            
            rowcount = tinfo.get("rowcount", None)
            if rowcount != None:
                schemaLines.append( " " )
                schemaLines.append( f"approximately {rowcount} rows")


            schemaLines.append( "{{{" )
            schemaLines.append( "CREATE TABLE %s (" % (t) )
            for coldef in tinfo['cols']:
                schemaLines.append( coldef )

            schemaLines.append( ")" )

            comments = tinfo.get( "comments" )
            if comments:
                schemaLines.append( " " )
                for comment in comments:
                    schemaLines.append( "%s" % comment )
                schemaLines.append( " " )                    

            schemaLines.append( " " )
            
            schemaLines.append( "}}}" )
            schemaLines.append( " " )

        return "\n".join(schemaLines)

class TracPoster(WikiPoster):

    def __init__(self,  postUrl, username, password):
        self.postUrl=postUrl
        self.username=username
        self.password=password

    def post(self, schemaText):
        import mechanize
        b = mechanize.Browser()
        import base64

        #b.addheaders.append(('Authorization', 'Basic %s' % base64.encodestring('%s:%s' % (self.username, self.password))) )
        b.add_password(self.postUrl, self.username, self.password)
        b.set_handle_robots(False)

        p = b.open( self.postUrl )
        print( p.read() )

        b.select_form( nr=1 )
        #b["action"]="edit"
        #b["scroll_bar_pos"]="0"
        b["text"] = schemaText # "Boo Voodoo"
        #b["save"] = "Submit changes"
        b["author"] = "schemaToWiki"
        b["comment"] = "%s" % datetime.now()

        b.set_debug_http(1)
        p = b.submit(name='save', label='Submit changes')
        #r = b.form.click(name='save')
        #print help(r)
        #print r.get_full_url()
        #print r.header_items()
        #print r.get_data()
        
        #p = b.open( r )
        print( p.read() )

        print( p )

        print( "Wiki updated!" )
        

