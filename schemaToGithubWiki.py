#!/usr/bin/env python

"""
 Copy schema definitions to a redmine wiki.
"""

from schemaToWiki import SchemaPoster, buildconfig
from github import GithubPoster, GithubFormatter

if __name__ == '__main__' :
    config = buildconfig(requireWikiCreds=False)

    print( "=" * 60 )
    print( config.__file__ )
    print( "=" * 60 )

    print( config.showqueries )


    postUrl="https://github.org"

    formatter = GithubFormatter()
    poster = GithubPoster( ) # postUrl=postUrl, username=username, password=password)

    man = SchemaPoster(connstr=config.connstr, config=config, formatter=formatter, poster=poster, schema=config.schema)
    man.postSchema()
