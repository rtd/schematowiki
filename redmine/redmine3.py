
from schemaToWiki import WikiPoster

from utils import *

# Ignore ssl cert errors.
import ssl
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    # Legacy Python that doesn't verify HTTPS certificates by default
    pass
else:
    # Handle target environment that doesn't support HTTPS verification
    ssl._create_default_https_context = _create_unverified_https_context

class RedminePoster(WikiPoster):
    """Support Redmine 3.4.2"""

    def __init__(self, baseUrl, postUrl, username, password):
        self.baseUrl=baseUrl
        self.postUrl=postUrl
        self.username=username
        self.password=password

    def post(self, schemaText):
        import mechanize
        b = mechanize.Browser()
        b.set_handle_robots(False)
        loginurl="%s/login" % self.baseUrl
        loginurl=loginurl.replace("//login","/login")
        print(loginurl)
        fd = b.open(loginurl)
        
        #b.select_form( nr=2 )
        print( "login page forms = %s:" % len( b.forms() ) )
        b.select_form( nr=0 )
        b["username"] = self.username
        b["password"] = self.password
        p = b.submit()

        p = b.open( self.postUrl )
        #print( p.read() )

        thetext = stripfunky( schemaText )

        logfile="schemaText.out"
        with open( logfile, "w") as outf:
            outf.write( thetext  )

        print( "Wrote %s bytes to schemaText to %s (for review if the post fails)" % (len(schemaText), logfile) )

        b.select_form( nr=2 )
        b["content[text]"] = thetext
        p = b.submit()

        print( "Wiki updated!" )
        
#^
