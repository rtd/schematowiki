
from schemaToWiki import WikiPoster

class RedminePoster(WikiPoster):

    def __init__(self, baseUrl, postUrl, username, password):
        self.baseUrl=baseUrl
        self.postUrl=postUrl
        self.username=username
        self.password=password

    def post(self, schemaText):
        import mechanize
        b = mechanize.Browser()
        fd = b.open("%s/login" % self.baseUrl)
        
        b.select_form( nr=1 )
        b["username"] = self.username
        b["password"] = self.password
        p = b.submit()

        p = b.open( self.postUrl )
        print( p.read() )

        b.select_form( nr=1 )
        b["content[text]"] = schemaText
        p = b.submit()

        print( "Wiki updated!" )
        
#^
