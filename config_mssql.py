
"""
Copy this file to config.py

Edit the file to contain your WIKI credientials and your PG login info.

Run the schemaToRedmine.py (or whatever your wiki is)

./schemaToWiki.py -f config

If the configuration is correct your wiki page should contain the Markdown for
  your schema.
"""

showqueries=False
connstr="mssql+pymssql://youruser:yourpass@mssqlhost/mssqldatabase"
schema="dbo"

wikiBaseUrl="https://myredminewiki.foo.com/"
wikiPostUrl="https://myredminewiki.foo.com/projects/myredmine/wiki/Actual_Schema/edit" 
wikiUsername="yourredmineuser"
wikiPassword="yourredminepass"
