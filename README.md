# schemaToWiki

## What is it?

A utility script to read your schema from a database and output to a WIKI page. This
lowers the bar to "query" for fields present in the database and makes it a little
easier to browse the tables in the database.

## How to use it?

`
pip install -r requirements.txt

cp config_example.py config.py
edit config.py # Supply your WIKI and PG credientials

# Run it to post your schema
./schemaToWiki.py -f config
`

If everything goes well, the schema will be posted to your WIKI page and Bob is your uncle!
