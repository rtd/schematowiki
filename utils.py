# -*- coding: latin-1 -*-

from __future__ import print_function

from urllib.parse import urlparse
import re
import requests
import hashlib
import nltk
import json
import html2text
import tldextract
#import urllib2
import requests
import uuid
from decimal import Decimal

from bodhala.core.timer import Timers
from bodhala.core import bodhala_config

def isstring(obj):
    return type(obj) in [ str, unicode ]


"""Converts a value to unicode, even it is already a unicode string.
    >>> from Products.CMFPlone.utils import safe_unicode
    >>> safe_unicode('spam')
    u'spam'
    >>> safe_unicode(u'spam')
    u'spam'
    >>> safe_unicode(u'spam'.encode('utf-8'))
    u'spam'
    >>> safe_unicode('\xc6\xb5')
    u'\u01b5'
    >>> safe_unicode(u'\xc6\xb5'.encode('iso-8859-1'))
    u'\u01b5'
    >>> safe_unicode('\xc6\xb5', encoding='ascii')
    u'\u01b5'
    >>> safe_unicode(1)
    1
    >>> print safe_unicode(None)
    None
"""
def safe_unicode(value, encoding='utf-8', errors="replace"):
    if isinstance(value, unicode):
        return value
    elif isinstance(value, basestring):
        try:
            value = unicode(value, encoding)
        except (UnicodeDecodeError):
            value = value.decode('utf-8', errors=errors)
    return value


def startTimers(timedStr, timers=None):
    if timers is None:
        timers = Timers()
    timers.start(timedStr)

    return timers

def stopTimers(timers, timedStr):
    timers.stop(timedStr)
    if bodhala_config.debugLevel>7:
        timers.show()

def isblank(s):
    if s is None:
        return True

    if type(s) == list:
        try:
            s=" ".join(s)
        except:
            # If this fails the list is likely not of strings return false
            return False

    if type(s) in [uuid.UUID, int, float, Decimal]:
        return False

    if s:
        if type(s) in [unicode, str]:
            if not(s) or s.isspace():
                return True

        
        return False

    return True

def islist(obj):
    if isin( type(obj), [list,tuple]):
        return True

    #if hasattr( obj, '__iter__'):
    #    return True

    return False

def isiterable(obj):
    if isin( type(obj), [list,tuple] ):
        return True

    if hasattr( obj, '__iter__'):
        return True

    if hasattr( obj, "collection"):
        return True

    return False


def getHashOfURL(url):
    r = requests.get(url)
    if r.status_code == requests.codes.ok:
        text = r.text.encode('ascii', 'ignore')
        clean_text = nltk.clean_html(text)
        
        hash = hashlib.md5(clean_text).hexdigest()
    else:
        r.raise_for_status()
    
    return hash
    
    
    
def getVisibleText(htmlfrag):
    timersStr = "getVisibleText"
    #timers = startTimers(timersStr)
    text=htmlfrag
    try:
        if htmlfrag and hasHtml(htmlfrag):
            h = html2text.HTML2Text()
            h.ignore_links = True

            items = h.handle(htmlfrag)
        
            if islist( items ):
                text = " ".join(items)
            else:
                text = items
            # TODO: html2text adds # signs for LI and other structured HTML elements. Which means we are squashing real pound signs. :-(
            text = text.replace( "#", " " ).replace( "*", " " ).strip()
    except:
        print( "** getVisibleText fails on %s **" % htmlfrag )
        raise
        return htmlfrag
        
    #stopTimers(timers, timersStr)
    return text


TAGRE=re.compile( ".*?<([A-Z][A-Z0-9]*)\b*.*>.*", re.IGNORECASE )
def hasHtml( fldval):
    hasit = False
    if fldval:
        if not(isin( type(fldval), [ list,str,unicode ] ) ):
            return False

            
        if islist( fldval ):
            for fval in fldval:
                hasit = hasHtml( fval )
                if hasit:
                    return hasit
        else:
            m = None
            try:
                m = TAGRE.match( fldval ) 
            except:
                print( "hasHtml Exception for val(%s)=%s" % (type(fldval), fldval) )
                raise
            if m:
                return True
    return False


def normalizeSpace(text):
    """Replace multiple white space chars with a single space."""

    if text:
        # Replace newlines and tabs with spaces.
        text = text.replace( "\r", " ").replace( "\t", " ").replace( "\n", " ")

        return " ".join( text.split() )
    
    return text

def stripNumbers(text):
    """Strip out numbers from a string."""

    newtext=[]
    for ch in text:
        if not(ch.isdigit()):
            newtext.append( ch )
            
    return "".join( newtext )

def isin( val, listofvals):
    for v in listofvals:
        if val == v:
            return True

    return False

def gettext( val ):
    text=val

    if val:
        if type(val) == list:
            text=" ".join(val)

    return text

def match( regexlist, str):
    """Given a list of compiled regex expressions, see if a string matches any of the expressions."""

    for r in regexlist:
        m = r.match( gettext(str) ) 
        if m:
            return m

    return None

def getBaseDomainName(url):
    parts= tldextract.extract(url)
    return "%s.%s" % (parts.domain, parts.suffix)

def getSubDomain(url):
    parts= tldextract.extract(url)
    return parts.subdomain

def getBaseUrl(url):
    domain = getBaseDomainName(url)
    scheme = getScheme(url)
    subdomain = getSubDomain(url)
    if subdomain:
        return "%s://%s.%s" % (scheme, subdomain, domain)
    else:
        return "%s://%s" % (scheme, domain)

def getScheme(url):
    if url.startswith("https"):
        return "https"
    else:
        return "http"

def getUri(url):
    dn = getBaseDomainName(url)
    parsed = urlparse(url)
    uri = "%s%s" % (dn, parsed.path)
    if parsed.query:
        uri = "%s%s%s" % (uri, "?", parsed.query)

    return uri

def getPath(url):
    parsed = urlparse(url)
    return parsed.path

def getActualUrl(url):
    # Use the requests lib instead
    actualurl=url
    resp = requests.head( url )
    if resp.history:
        # Request was redirected
        for r in resp.history:
            actualurl=r.url

    return actualurl
    """
    req = urllib2.Request( url )
    req.get_method = lambda : 'HEAD'
    res = urllib2.urlopen( req )
    if res is not None:
        return res.geturl()
    """

def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)

def getPath(url):
    parsed = urlparse(url)
    return parsed.path

def getHostname():
    import os
    return os.uname()[1].split(".")[0]

def minDate(date1, date2):
    if date1 is None and date2 is None:
        return None
    if date1 is None and date2 is not None:
        return date2
    elif date2 is None and date1 is not None:
        return date1
    else:
        return date1 if date1 > date2 else date2

def shouldRefreshLfUtils(lfutils, crawlId, lawFirmId):
    if lfutils is None:
        return True
    if crawlId and lfutils.getCrawlId():
        return crawlId != lfutils.getCrawlId()
    if lawFirmId and lfutils.getLawFirmId():
        return lawFirmId != lfutils.getLawFirmId()

    return True

def all_fields_exist(d_ict, fields):
    for field in fields:
        if d_ict.get(field, None) is None:
            return False

    return True

def all_fields_exist(l_ist):
    for l in l_ist:
        if l == None:
            return False

    return True

'''If the return value is a string, this decorator initCaps it'''
def init_cap(func):
    def func_wrapper(*args, **kwargs):
        result = func(*args)
        if result is not None and type(result) is str:
            return result.title()
        else:
            return result

    return func_wrapper

def dump_args(func):
    "This decorator dumps out the arguments passed to a function before calling it"

    argnames = func.func_code.co_varnames[:func.func_code.co_argcount]
    fname = func.func_name

    def echo_func(*args,**kwargs):
        from bodhala.core.log import isdebug
        try:
            if isdebug():
                print( fname, ":", ', '.join(
                    '%s=%r' % entry
                    for entry in zip(argnames,args) + kwargs.items()
                ))
        except:
            print( "%s: Exception dumping args for method call." % fname )
            pass
        return func(*args, **kwargs)

    return echo_func


#from datetime import datetime, tzinfo, timedelta
import datetime
import math
class bodhala_tz(datetime.tzinfo):
    def tzname(self):
        """This class builds an accurate Timezone description for a current time. If applied to a time in the past, this will define an inaccurate TZ description.
           How to use this:
              datetime.utcnow().replace(tzinfo=bodhala_tz()).isoformat()
"""
        return "UTC"
    def utcoffset(self, dt):
        un=datetime.datetime.utcnow()
        n=datetime.datetime.now()
        td=un-n
        return datetime.timedelta(seconds=math.ceil(td.seconds/60.0)*60)  # timedelta( seconds=(3600*5) )

def getUtfText(text, errors="ignore"):
    kosher = None

    # Maybe we have clean text?
    try:
        kosher = safe_unicode(text, encoding="utf-8", errors=errors)
    except:
        pass

    if not(kosher) and text:
        try:
            text = text.decode( "windows-1252", errors=errors )
            kosher = text.encode("utf-8", errors=errors)
        except:
            raise

    return kosher

def nvl(val, defval):
    """Emulate Oracle's pl/sql nvl procedure."""
    if val == None or isblank(val):
        return defval
    return val

DEACCENT = {
    #                 ASCII      UTF8
    "\xe2\x80\x9c" : [ '"',    "\x201C" ],     # 93 - LEFT DOUBLE QUOTATION MARK
    "\x93\x94"     : [ '"',    "\x201C" ],     # 93 - DOUBLE QUOTATION MARK    
    "\x91\x92"     : [ "'",    "\x2019" ],     # 91 - SINGLE QUOTATION MARK    
    "\x85"         : [ '. . .',". . ." ],      # 85 - FUNKY ELIPSIS
    "\xa0"         : [ ' ',    " " ],          # A0 - FUNKY SPACE
    "’"            : [ "'",    "'"],           # Back Tick
    "\xe2\x80\x99" : [ "'",   "\x2019" ],     # 92 - RIGHT SINGLE QUOTATION MARK
    "\xe2\x80\x93" : [ '-',    "\x2013" ],     # 96 - EN DASH
    "\xe2\x80\x9d" : [ '"',    "\x201D" ],     # 94 - RIGHT DOUBLE QUOTATION MARK
    # --- the rest are from moronic, try to combine the two.
    "\xe2\x80\x9a" : [ ',',    "\x201A" ],     # 82 - SINGLE LOW-9 QUOTATION MARK
    "\xe2\x80\x9e" : [ ',,',   "\x201E" ],     # 84 - DOUBLE LOW-9 QUOTATION MARK
    "\xe2\x80\xa6" : [ '...',  "\x2026" ],     # 85 - HORIZONTAL ELLIPSIS
    "\xcb\x86"     : [ '^',    "\x02C6" ],     # 88 - MODIFIER LETTER CIRCUMFLEX ACCENT
    "\xe2\x80\x98" : [ '`',    "\x2018" ],     # 91 - LEFT SINGLE QUOTATION MARK
    "\xe2\x80\xa2" : [ '*',    "\x2022" ],     # 95 - BULLET
    "\xe2\x80\x94" : [ '-',    "\x2014" ],     # 97 - EM DASH

    "\xe2\x80\xb9" : [ '<',    "\x2039" ],     # 8B - SINGLE LEFT-POINTING ANGLE
                                                #      QUOTATION MARK
    "\xe2\x80\xba" : [ '>',    "\x203A" ]      # 9B - SINGLE RIGHT-POINTING ANGLE
                                               #      QUOTATION MARK   
}

DEACCENT_UTF = {
    #                 ASCII      UTF8
    u"\xe2\x80\x9c" : [ '"',    u"\u201C" ],     # 93 - LEFT DOUBLE QUOTATION MARK
    u"\x93\x94"     : [ '"',    u"\u201C" ],     # 93 - DOUBLE QUOTATION MARK    
    u"\x91\x92"     : [ "'",    u"\u2019" ],     # 91 - SINGLE QUOTATION MARK    
    u"\x85"         : [ '. . .',u". . ." ],      # 85 - FUNKY ELIPSIS
    u"\xA0"         : [ ' ',    u" " ],          # A0 - FUNKY SPACE
    u"’"            : [ "'",    u"'"],           # Back Tick
    u"\xe2\x80\x99" : [ "'",    u"\u2019" ],     # 92 - RIGHT SINGLE QUOTATION MARK
    u"\xe2\x80\x93" : [ '-',    u"\u2013" ],     # 96 - EN DASH
    u"\xe2\x80\x9d" : [ '"',    u"\u201D" ],     # 94 - RIGHT DOUBLE QUOTATION MARK
    # --- the rest are from moronic, try to combine the two.
    u"\xe2\x80\x9a" : [ ',',    u"\u201A" ],     # 82 - SINGLE LOW-9 QUOTATION MARK
    u"\xe2\x80\x9e" : [ ',,',   u"\u201E" ],     # 84 - DOUBLE LOW-9 QUOTATION MARK
    u"\xe2\x80\xa6" : [ '...',  u"\u2026" ],     # 85 - HORIZONTAL ELLIPSIS
    u"\xcb\x86"     : [ '^',    u"\u02C6" ],     # 88 - MODIFIER LETTER CIRCUMFLEX ACCENT
    u"\xe2\x80\x98" : [ '`',    u"\u2018" ],     # 91 - LEFT SINGLE QUOTATION MARK
    u"\xe2\x80\xa2" : [ '*',    u"\u2022" ],     # 95 - BULLET
    u"\xe2\x80\x94" : [ '-',    u"\u2014" ],     # 97 - EM DASH

    u"\xe2\x80\xb9" : [ '<',    u"\u2039" ],     # 8B - SINGLE LEFT-POINTING ANGLE
                                                 #      QUOTATION MARK
    u"\xe2\x80\xba" : [ '>',    u"\u203A" ]      # 9B - SINGLE RIGHT-POINTING ANGLE
                                                 #      QUOTATION MARK   
}
#DEACCENT_UTF = {}
#for k in DEACCENT.keys():
#    encoded = k #.encode('utf8')
#    DEACCENT_UTF[ unicode( encoded ) ] = DEACCENT[k]
#    print("UTF Mapping made: %s" % k )

# Build a map for unicode chars that come in via a ASCII code
UNICODE_MAP={}
for k in DEACCENT.keys():
    uchar = DEACCENT[k][1] 
    if len(uchar)==1:
        UNICODE_MAP[ ord( uchar ) ] = DEACCENT[k][0]
    
DEACCENT_RE = re.compile( "(" + "|".join( DEACCENT.keys() ) + ")" )

def cleantext(s, encode=None):
    """Preferred name for deaccent. deaccent will be deprecated soonish."""

    if type(s) in [ str, unicode ]:
        return deaccent(s, encode)
    
    return cleantext( "%s" % s )

def stripfunky(str):
    newstr=[]
    for ch in str:
        if ord(ch)<128:
            newstr.append(ch)
        else:
            trchar=UNICODE_MAP.get( ord(ch), None)
            if trchar:
                newstr.append(trchar)
                
    return "".join( newstr )
    #|

def deaccent(s, encode=None):
    """Normalize the text we procure from the Internet. Remove funky chars and such. 
       This is a port of Jim's Perl Function of the same name."""

    # TODO: Deprecate this method in favor of cleantext above.
    if not(isstring(s)) and isiterable(s):
        # iterate over the list item and clean any text.
        newlist = []
        for item in iterate(s):
            newlist.append( deaccent( item, encode ) )
        return newlist

    uni=False
    deaccentMap=DEACCENT
    
    def myreplace(match):
        if match:
            rep=0
            if uni:
                rep=1
            try:
                m=match.group(0)
                if len(m)>0:
                    return deaccentMap.get( m )[rep]
            except:
                print("** %s lookup in DEACCENT fails. unicode=%s**" % (repr(match.group(0)), uni) )
                kdsp=[]
                for k in DEACCENT.keys():
                    kdsp.append( repr(k) )
                
                print("** Check it: %s **" % ("(" + "|".join( kdsp ) + ")") )
                raise
        return match.group(0)

    
    ret = s
    if type(ret) == unicode:
        # ret = s.decode( encoding='cp1252', errors='ignore' )
        ret = getUtfText( s )
        uni = True
        deaccentMap = DEACCENT_UTF

    if ret:
        #Normalize whitespace by sipping out CR chars
        ret = ret.replace( '\015\012',  '\012' )

    
        ret = DEACCENT_RE.sub( myreplace, ret)        
        ## Replace funky quotes
        ## 0x93 (147) and 0x94 (148) are "smart" quotes
        ## 0x91 (145) and 0x92 (146) are "smart" singlequotes
        ## 0x85 (133) is an ellipsis
        ## \xe2\x80\x99 = wide back tick
        ## \xe2\x80\x9c == wide double quote
        ## \xe2\x80\x93 == wide dash
        ## \xe2\x80\x9d == wide double quote
        #ret = ret.replace( '\xe2\x80\x9c', '"') \
        #         .replace( '\x93\x94', '"' )    \
        #         .replace( '\x91\x92', "'" )    \
        #         .replace( '\x85', '. . .')     \
        #         .replace( '\xA0', ' ' )        \
        #         .replace( '’', "'" )           \
        #         .replace( '\xe2\x80\x99', "'") \
        #         .replace( '\xe2\x80\x9c', '"') \
        #         .replace( '\xe2\x80\x93', '-') \
        #         .replace( '\xe2\x80\x9d', '"')   
        
        # Remove all unicode chars outside the normal range for which we don't have an explicit mapping
        #ret = re.sub( '[\x80-\xff]', '', ret )
        if not(uni) or encode=='ascii':
            ret = stripfunky( ret )
        if encode == 'ascii':
            ret = str(ret)

    return ret

MORONIC = {
    #                 ASCII      UTF8  
    "\xE2\x80\x9A" : [ ',',    "\x201A" ],     # 82 - SINGLE LOW-9 QUOTATION MARK
    "\xE2\x80\x9E" : [ ',,',   "\x201E" ],     # 84 - DOUBLE LOW-9 QUOTATION MARK
    "\xE2\x80\xA6" : [ '...',  "\x2026" ],     # 85 - HORIZONTAL ELLIPSIS
    "\xCB\x86"     : [ '^',    "\x02C6" ],     # 88 - MODIFIER LETTER CIRCUMFLEX ACCENT
    "\xE2\x80\x98" : [ '`',    "\x2018" ],     # 91 - LEFT SINGLE QUOTATION MARK
    "\xE2\x80\x99" : [ "'",   "\x2019" ],     # 92 - RIGHT SINGLE QUOTATION MARK
    "\xE2\x80\x9C" : [ '"',    "\x201C" ],     # 93 - LEFT DOUBLE QUOTATION MARK
    "\xE2\x80\x9D" : [ '"',    "\x201D" ],     # 94 - RIGHT DOUBLE QUOTATION MARK
    "\xE2\x80\xA2" : [ '*',    "\x2022" ],     # 95 - BULLET
    "\xE2\x80\x93" : [ '-',    "\x2013" ],     # 96 - EN DASH
    "\xE2\x80\x94" : [ '-',    "\x2014" ],     # 97 - EM DASH

    "\xE2\x80\xB9" : [ '<',    "\x2039" ],     # 8B - SINGLE LEFT-POINTING ANGLE
                                                #      QUOTATION MARK
    "\xE2\x80\xBA" : [ '>',    "\x203A" ]      # 9B - SINGLE RIGHT-POINTING ANGLE
                                               #      QUOTATION MARK   
}
MORONIC_RE = re.compile( "(" + "|".join( MORONIC.keys() ) + ")" )



def demoroniser(s):
    """Do you find your texts are plagued by moronic characters? If so use they demoroniser to clean them up. 
       Python text demoroniser inspired by Perl's Text::Demoroniser."""

    def myreplace(match):
        return MORONIC.get( match.group(0))[0]

    if s:
        return MORONIC_RE.sub( myreplace, s)
    return None

def demoroniser_utf8(s):
    """Do you find your UTF8 texts are plagued by moronic characters? If so use the demoroniser to clean them up. 
       Python text demoroniser inspired by Perl's Text::Demoroniser."""
    
    def myreplace(match):
        return MORONIC.get( match.group(0))[1]

    if s:
        return MORONIC_RE.sub( myreplace, s)
    return None


def iterate( elem_or_list):
    if getattr( elem_or_list, "iter", None):
        return elem_or_list.iter()
    if getattr( elem_or_list, "collection", None):
        # Mongo cursor
        return elem_or_list
    elif isinstance(elem_or_list, (list, tuple) ):
        return elem_or_list
    return []

def equalsUuid(uuid1, uuid2):
    """Sigh... the problem with uuids. Mongo likes to store them in a different way than Python likes to represent them.
       This method attempts to overcome these problems and return an accurate comparision of the two UUID representations."""

    # Going the SQL route, we don't know if None values are equal so we will return false.
    if uuid1 == None:
        return False
    if uuid2 == None:
        return False

    assert isin(type(uuid1), [str, unicode, uuid.UUID]), "uuid1 %s (%s) is not a str, unicode or uuid.UUID" % (uuid1, type(uuid1))
    assert isin(type(uuid1), [str, unicode, uuid.UUID]), "uuid2 %s (%s) is not a str, unicode or uuid.UUID" % (uuid1, type(uuid1))


    # Try the simple
    match = uuid1 == uuid2
    if match:
        return match

    uuid1val = uuid1
    if type(uuid1) is uuid.UUID:
        uuid1val = "%s" % uuid1
    if uuid1val:
        uuid1val=uuid1val.replace( "-", "" )

    uuid2val = uuid2
    if type(uuid2) is uuid.UUID:
        uuid2val = "%s" % uuid2
    if uuid2val:
        uuid2val = uuid2val.replace( "-", "" )

    match = uuid1val == uuid2val

    return match

def getfirst(obj):
    if obj and islist(obj):
        if len(obj)>0:
            return obj[0]
        else:
            None
    return obj

def scrub(s):
    """Remove non-alpha chars (including spaces and dashes) from a string."""

    if not(isblank(s)):
        scrubbed=[]
        for ch in s:
            if ch.isalpha() or ch in [ " ", "-" ]:
                scrubbed.append(ch)
                

        ret = "".join( scrubbed ).strip()
        return ret

    return s

def englishtext(t):
    """Remove non-english chars from a string. Deal with some anomalies in name for example dentons."""

    import string
    normaltext = string.printable + string.whitespace
    newt=[]
    for ch in t:
        if ch in normaltext:
            newt.append( ch )
            
    return "".join(newt)
        
        
def getDatetime(val):
    dtval = val
    # convert a date-like value into a datetime.datetime object

    if type(dtval) == datetime.datetime:
        return dtval

    if type(val) == datetime.date:
        dtval =  datetime.datetime( val.year, val.month, val.day )

    if type(val) in [ str, unicode ]:
        # JS date
        # TODO: add a regex to pick the most appropriate string conversion also does delorean have a roll to play here?
        dtval = datetime.datetime.strptime(val, '%Y-%m-%d')

    return dtval

