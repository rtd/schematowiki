#!/usr/bin/env python

"""
 Copy schema definitions to a redmine wiki.
"""

from schemaToWiki import SchemaPoster, TracPoster,  TracFormatter

if __name__ == '__main__' :
    config = buildconfig()

    print( "=" * 60 )
    print( config.__file__ )
    print( "=" * 60 )

    formatter = TracFormatter()
    poster = TracPoster( postUrl=config.postUrl, username=config.username, password=config.password)

    man = SchemaPoster(connstr=config.connstr, formatter=formatter, poster=poster)
    man.postSchema()
