#!/usr/bin/env python

from __future__ import print_function

import unittest


class TestRedmine(unittest.TestCase):

    def testRedmine(self):
        import config
        from redmine.redmine3 import RedminePoster

        rp = RedminePoster( config.wikiBaseUrl,  config.wikiPostUrl, config.wikiUsername, config.wikiPassword )

        rp.post( "Foo" )
        pass


if __name__ == "__main__":
    import sys
    sys.path.append( "." )
    print( sys.path )
    unittest.main()
    
