
from schemaToWiki import WikiFormatter

"""github MD support"""

class GithubFormatter(WikiFormatter):
    """Format the wiki entry Github/Textile style."""

    def format(self, tabinfo):
        """tabinfo is the data model of the meta data for the tables"""

        schemaLines=[]

        schemaLines.append("# Actual Schema")
        schemaLines.append(" ")

        for tinfo in tabinfo:
            t = tinfo.get('name',"")
            fks = {}

            schemaLines.append( "## %s" % t.upper() )
            rowcount = tinfo.get("rowcount", None)
            if rowcount != None:
                schemaLines.append( " " )
                schemaLines.append( f"approximately {rowcount} rows")
            
            schemaLines.append( " " )

            schemaLines.append( "```\nCREATE TABLE %s (" % (t) )
            for coldef in tinfo['cols']:
                schemaLines.append( coldef )

            comments = tinfo.get( "comments" )
            if comments:
                schemaLines.append( "\n" )
                for comment in comments:
                    schemaLines.append( "%s\n" % comment )
                schemaLines.append( "\n" )                    

            schemaLines.append( "\n" )                                

            schemaLines.append( ")\n```\n" )

        return "\n".join(schemaLines)

class GithubPoster(object):
    """To do figure out how to post to a github WIKI."""

    def post(self, schemaText):
        # TODO: Poor man's solution. Dump the text to a file and let the user know they need to post it.
        #       We can improve this later by posting directly with GIT?

        mdfile="wiki-post.md"
        with open(mdfile,"w") as of:
            of.write(schemaText)

        print( f'"{mdfile}" is the schema text you seek' )
        #print schemaText

