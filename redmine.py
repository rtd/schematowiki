
from schemaToWiki import WikiFormatter

"""Redmine wiki support."""

class RedmineFormatter(WikiFormatter):
    """Format the wiki entry Redmine style."""

    def format(self, tabinfo):
        """tabinfo is the data model of the meta data for the tables"""

        schemaLines=[]

        schemaLines.append("h1. Actual Schema")
        schemaLines.append(" ")
        schemaLines.append("Generated on %s by schemaToWiki %s." % (datetime.now(), "https://rtd@bitbucket.org/rtd/schematowiki.git") )
        schemaLines.append(" ")

        for tinfo in tabinfo:
            t = tinfo.get('name',"")
            fks = {}

            schemaLines.append( "h2. %s" % t.upper() )
            schemaLines.append( " " )

            rowcount = tinfo.get("rowcount", None)
            if rowcount != None:
                schemaLines.append( " " )
                schemaLines.append( f"approximately {rowcount} rows")

            commentdict = tinfo.get( "commentdict", {} )
            
            schemaLines.append( "<pre><code class='sql'>" )
            schemaLines.append( "-- %s" % commentdict.get( t.lower(), "" ) )
            schemaLines.append( "CREATE TABLE %s (" % (t) )
            for coldef in tinfo['cols']:
                # TODO: parsing sucks. Don't do this!
                colname = coldef.split()[0]
                _comment=""
                colcomment = commentdict.get( "%s.%s" % ( t.lower(), colname.lower() ), None ) # todo get the column name
                if colcomment:
                    _comment = "-- %s" % colcomment
                                              
                schemaLines.append( "%-40s, %s" % (coldef, _comment) )

            schemaLines.append( ")" )


            comments = tinfo.get( "comments" )
            if comments:
                schemaLines.append( " " )
                for comment in comments:
                    schemaLines.append( comment )
                schemaLines.append( " " )                    

            schemaLines.append( "</code></pre>" )
            schemaLines.append( " " )                
                

        return "\n".join(schemaLines)
