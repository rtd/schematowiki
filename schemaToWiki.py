#!/usr/bin/env python

from __future__ import print_function

"""
 Copy schema definitions to a redmine wiki.
"""


    
import sys
from datetime import datetime
from sqlalchemy import create_engine, inspect, MetaData, text
import tqdm

def loadConfigFile(configpath):
    """Pure gold import a python file for it's configuration value."""

    cfgpath = configpath.replace( ".py", "" ) # If the user provides a .py extension strip it for them
    
    __import__(cfgpath)
    return sys.modules[ cfgpath ]


class WikiFormatter(object):
    """Formats the table info into a WIKI mark up."""
    
    def format(self,tabinfo):
        """tabinfo is the data model of the meta data for the tables"""
        pass

class WikiPoster(object):
    """Posts the WIKI text to a particular wiki page."""

    def __init__(self, postUrl):
        self.postUrl = postUrl

    def post(self, text):
        pass
        

class InspectorMetaDataExtractor(object):

    def __init__(self, inspector, engine, config):
        self.inspector = inspector
        self.engine = engine
        self.config = config

    def getMetaData(self):
        """tabinfo is the data model of the meta data for the tables"""
        print(  self.engine.dialect.name )
        print( f"showqueries='{self.engine.echo}'")


        tabinfo = []
        tabs=[]
        #help(self.inspector.get_table_names)
        for t in tqdm.tqdm(self.inspector.get_table_names()):
            tabs.append(t)

        """
        pb = tqdm.tqdm( total=len(tabs),
                        desc="Fetching meta data.",
                        mininterval=0.1,
                        miniters=1)"""

        table_filter = {}
        if self.config and hasattr( self.config, "table_filter" ):
            for t in self.config.table_filter:
                table_filter[ t.lower() ] = True

        done=0
        tabs.sort()
        for t in tqdm.tqdm(tabs):
            include=True
            if len(table_filter):
                include=False
                if table_filter.get( t.lower() , False):
                    include=True
                    
            if not(include):
                continue
            fks = {}
            fklist = self.inspector.get_foreign_keys( t )
            for fk in fklist:
                fkname = fk.get('constrained_columns',[None])[0]
                reftable = fk.get('referred_table',None)
                refcol = fk.get('referred_columns',[None])[0]

                if reftable and refcol and fkname:
                    fks[fkname] = "REFERENCES %s(%s)" % (reftable, refcol)

            tinfo = {}
            tinfo['name'] = t

            rowcount=None
            if self.config and hasattr( self.config, "include_rowcounts"):
                try:
                    rowcount = self.inspector.get_rowcount(t)
                except:
                    raise
            tinfo['rowcount']=rowcount


            coldefs=[]
            for c in self.inspector.get_columns(t):
                fkdetail = ""
                fkinfo = fks.get( c.get('name', None), None)
                if fkinfo:
                    fkdetail = fkinfo

                coldef = "  %(name)s %(type)s %(null)s %(fk)s" % { 
                    'name': c.get('name',''), 
                    'type': c.get('type',''), 
                    'null': "" if c.get('nullable', True) else "NOT NULL",
                    'fk': fkdetail
                    } 
                coldefs.append( coldef )

            tinfo['cols']=coldefs

            if self.engine.dialect.name == "postgresql":
                # Fetch comment info for tables.
                oidres = self.engine.execute( f"select c.oid from pg_catalog.pg_class c WHERE c.relname = '{t.lower()}'" ).first();

                toid = None
                if oidres:
                    toid = oidres[0]
                
                #res = self.engine.execute( "select obj_description('public.%s'::regclass, 'pg_class')" % (t) )
                    comments = []
                    commentdict = {}
                    if toid:
                        # get the comment for the table oid
                        res = self.engine.execute( f"select pg_catalog.obj_description({toid});" )
                        row = res.first()
                        commentdict[ "%s" % t.lower() ] = row[0]
                        comments.append( "COMMENT ON TABLE %s IS '%s';" % (t, row[0]) )

                        colidx=1
                        for c in self.inspector.get_columns(t):
                            res = self.engine.execute( f"select col_description({toid}, {colidx})"  )
                            row = res.first()
                            if row[0]:
                                comments.append( "COMMENT ON COLUMN %s.%s IS '%s';" % (t, c.get('name'), row[0] ) )
                                commentdict[ "%s.%s" % ( t.lower(), c.get('name').lower() ) ] = row[0]
                            colidx += 1
                        tinfo['comments'] = comments
                        tinfo['commentdict'] = commentdict
                    

            tabinfo.append(tinfo)
            #pb.update( 1 ) #if done<1 else done )
            done=done+1
        #pb.update( len(tabs) )
        print( " " )



        return tabinfo


class MetaDataInspector(object):
    def __init__(self, md, engine):
        self.md = md
        self.engine = engine

    def get_table_names(self):
        return self.md.tables.keys()

    def get_foreign_keys(self, tname):
        t = self.md.tables.get(tname, None)
        #if t != None:
        #    return t.foreign_keys
        #else:
        #    return []
        return []

    def get_columns(self, tname):
        t = self.md.tables.get(tname, None)
        cols = []
        if t != None:
            for c in t.columns:
                #print c
                #print c.name
                #print c.type
                #print dir(c)
                cols.append( { 'name': c.name, 'type': c.type } )
            pass

        return cols
    
    def get_rowcount(self, tname):
        """Optionally count the number of rows in the database."""
        rowcount = None
        # TODO: use dialect to detect SQL Server
        mytname = '"'+tname+'"'
        
        if "dbo." in tname:
            tcomps=tname.split(".")
            mytname=".".join( [ f"[{tcomps[0]}]", f"[{tcomps[1]}]" ] )

        sql = f"""SELECT COUNT(1) FROM {mytname} """
        #print(sql)

        with self.engine.begin() as conn:
            rowcount = 0
            row = conn.execute( text(sql) ).first()

            if row:
                rowcount = row[0]


        return rowcount


class MetaDataExtractor(object):

    def __init__(self, md, engine, config):
        self.md = md
        self.engine = engine
        self.config = config

    def getMetaData(self):
        """tabinfo is the data model of the meta data for the tables"""

        imde = InspectorMetaDataExtractor(MetaDataInspector( self.md, self.engine ), self.engine, self.config)
        return imde.getMetaData()       

class SchemaPoster(object):
    """Coordinates the work of formatting the schema and posting it to a wiki."""

    def __init__(self, connstr=None, config=None, formatter=None, poster=None, schema=None):
        self.connstr = connstr
        showqueries=False
        try:
            showqueries=config.showqueries
        except:
            pass
        self.engine = create_engine(self.connstr, echo=showqueries)
        self.config = config
        print( f"Connected? schema={schema}" )
        if schema:
            md = MetaData()
            print( "MetaData.reflect doing it's thing (slowly?). " )
            md.reflect(bind=self.engine,schema=schema)
            self.inspector = None
            self.md = md
            #self.inspector = inspect( md )
        else:
            self.md = None
            self.inspector = inspect(self.engine)

        self.formatter = formatter
        self.poster = poster

    def getExtractor(self):
        if self.md:
            return MetaDataExtractor(self.md, engine=self.engine, config=self.config)

        if self.inspector:
            return InspectorMetaDataExtractor(self.inspector, self.engine, self.config)

    def getMetaData(self):
        return self.getExtractor().getMetaData()

    def postSchema(self):
        print("getMetaData")
        tabinfo = self.getMetaData()
        print("getMetaData finished")

        schemaText = self.formatter.format( tabinfo)
        print("posting")        
        self.poster.post( schemaText )
        print("post done")        

def buildconfig(requireWikiCreds=True):
    """Build the config from command line arguments."""

    import argparse

    ap = argparse.ArgumentParser(description='Utility to extract a Schema from a database and post it to a WIKI')
    ap.add_argument('-f','--configfile', help='Optional config file. Uses config.py by default.', default='config')

    args = ap.parse_args()

    config = loadConfigFile( args.configfile )

    # Check the config to see if expected elements were populated
    assert config.connstr != None, f"connstr should be defined in the config file! {args.configfile}"
    if requireWikiCreds:
        assert config.wikiUsername != None, f"username should be define in the config file! {args.configfile}"
        assert config.wikiPassword != None, f"password should be define in the config file! {args.configfile}"

    return config
    




