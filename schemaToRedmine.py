#!/usr/bin/env python

from __future__ import print_function

"""
 Copy schema definitions to a redmine wiki.
"""

import argparse

from schemaToWiki import SchemaPoster, loadConfigFile, buildconfig
from redmine import RedmineFormatter
from redmine.redmine3 import RedminePoster
import config


def main():
    config = buildconfig()

    print( "=" * 60 )
    print( config.__file__ )
    print( "=" * 60 )
    
    formatter = RedmineFormatter()
    poster = RedminePoster(baseUrl=config.wikiBaseUrl, postUrl=config.wikiPostUrl, username=config.wikiUsername, password=config.wikiPassword)

    print( config.connstr )

    man = SchemaPoster(connstr=config.connstr, config=config, formatter=formatter, poster=poster)
    man.postSchema()

if __name__ == '__main__' :
    main()




